update fbo_application_kob_products set 
fpvs_product_name='Nutraceutical (Green Tea, Amia, Haldi as Powder, Kasundi, Milk Thistle, Beetroot, Ginger)'
where product_mapping_id=37435380

update fbo_application_kob_products set 
fpvs_product_name='Nutraceutical (Ashwagandha, Brahmi, Tagar, Nardostachys Jatamansi, Matricaria Recutita, Salvia Officinalis)'
where product_mapping_id=39223341

update fbo_application_kob_products set 
fpvs_product_name='Nutraceutical (Arjuna, Brahmi, Dalchini, Lahsuna, Garlic, Howthorn, Ubiqinone and Unbiqionol Content)'
where product_mapping_id=39223200

update fbo_application_kob_products set 
fpvs_product_name='Nutraceutical (Neem, Giloy, Haldi as Powder, Manjistha, Tulsi, Red Sandalwood, Rosmarinus Officinalis)'
where product_mapping_id=39223468

update fbo_application_kob_products set 
fpvs_product_name='Nutraceutical (Vasaka, Haldi, Tulsi, Mulethi, Kakarsingi, Turmeric, Ginger, Lungwort)'
where product_mapping_id=39223484

update fbo_application_kob_products set 
fpvs_product_name='Nutraceutical (Kawnch Beej, Gokhru, Ginsing, Yohimbine) '
where product_mapping_id=39223506

update fbo_application_kob_products set 
fpvs_product_name='Nutraceutical (Punernava, Gokhru, Marshmellow Root, Plash, Varuna, Bergenia Cilicate)'
where product_mapping_id=39223372

update fbo_application_kob_products set 
fpvs_product_name='Nutraceutical (Salai Gggal, Kundru, Atasi, Adrakh, Ginger, Turmeric, Pisum Sativum L, Piper Nigrum, Rosehip) ',
where product_mapping_id=39223725

update fbo_application_kob_products set 
fpvs_product_name='Nutraceutical (Ashok,Sitawar,Chaste Tree Berry,Cimicifuga Racemosa,Wild Yam,Damiana Turneria Diffusa)',
where product_mapping_id=39223798

update fbo_application_kob_products set 
fpvs_product_name='Nutraceutical (Amlaki, Bringraj, Hibisus Rosa-SSinensis L. Flower, Thymus Linearris Benth, Rosmarinus Officinalis) ',
where product_mapping_id=39223457

update fbo_application_kob_products set 
fpvs_product_name='Nutraceutical (Kawnch Beej, Haldi, Ashwagandha, Safed Musali, Green Tea, Turmeric, Grape Seed Extract)',
where product_mapping_id=39223410

update fbo_application_kob_products set 
fpvs_product_name='Nutraceutical ( Giloy, Tulsi, Dalchini, Ashwagandha, Zingiber Officinale Rosc, Amia, Haldi, Curcuma Longa)',
where product_mapping_id=39223651

update fbo_application_kob_products set 
fpvs_product_name='Nutraceutical (Tamlaki, Mulethi, Milk Thistle, Bhuiamalaki, Kurchi, Glycyrrhiza Glabra, Kalmegh Green Chiretha Andrographis Paniculata)',
where product_mapping_id=39223788

update fbo_application_kob_products set 
fpvs_product_name='Nutraceutical (Amlaki, Neem, Manjistha, Giloy, Hadi)',
where product_mapping_id=39223671

update fbo_application_kob_products set 
fpvs_product_name='Nutraceutical (M. Concanensis Nimmo, Brahmi, Sitawar, Bilberry, Vitis Vinifera, Sea Buckthorn Berry, Goj Berry, Amla, Mangosteen, Wheat Grass, Aloe Vera, Blueberry) ',
where product_mapping_id=39223833




