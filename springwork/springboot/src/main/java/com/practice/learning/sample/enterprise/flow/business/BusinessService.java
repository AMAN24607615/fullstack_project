//businesslogic
package com.practice.learning.sample.enterprise.flow.business;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.practice.learning.sample.enterprise.flow.data.DataService;

@Component
public class BusinessService{
	@Autowired
	private DataService dataretrieve;
	public long calculateSum() {
		List<Integer> data = dataretrieve.retrieveData();
		return data.stream().reduce(Integer::sum).get();
	}
}
