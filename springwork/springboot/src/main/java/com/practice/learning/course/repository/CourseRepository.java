package com.practice.learning.course.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.practice.learning.course.bean.Course;

								// spring data jpa class //Entity ,ID data type							
public interface CourseRepository extends JpaRepository<Course,Long> {
	
}
