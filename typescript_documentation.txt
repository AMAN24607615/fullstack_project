================================Typescript-Array==================================
var array:string [];
array = ["1","2","3","4"];
console.log(array[0]);
console.log(array[1]);

var num:number []
num = [1,2,3,4,5,6];
console.log(num[0]);
console.log(num[1]);

var arr_names:number[] = new Array(4) 
for(var i = 0;i<arr_names.length;i++) 
{ 
   arr_names[i] = i * 2 
   console.log(arr_names[i]) 
}

var names:string[] = new 
Array("Mary","Tom","Jack","Jill") 
for(var i = 0;i<names.length;i++) 
{ 
   console.log(names[i]) 
   }
   
 
let num1 = [7, 8, 9]; 
num.forEach(function (value) { 
   console.log(value);
    });



var multi:number[][] = [[1,2,3],[23,24,25]] 
console.log(multi[0][0]) 
console.log(multi[0][1]) 
console.log(multi[0][2]) 
console.log(multi[1][0]) 
console.log(multi[1][1]) 
console.log(multi[1][2])

------------------------------------Tuple------------------------------------

syntax : var tuple_name = [value1,value2,value3,…value n]

var tup = [] 
tup[0] = 12 
tup[1] = 23 
console.log(tup[0]) 
console.log(tup[1]) 



var mytuple = [10,"Hello","World","typeScript"]; 
console.log("Items before push "+mytuple.length) 


var k = [10,"Aman"]
var [a,b] = k
console.log(a)
console.log(b)

-----------------------------------------------Union---------------------------------

syntax :Type1|Type2|Type3 


var arr1:number[]|string[]; 
var i:number;
arr1 = [1,2,4]
console.log("**numeric array**") 
for(i = 0;i<arr1.length;i++) { 
   console.log(arr1[i]) 
   } 
arr1 = ["Mumbai","Pune","Delhi"] 
console.log("**string array**") 
for(i = 0;i<arr1.length;i++)
 {
     console.log(arr1[i]) 
} 
//-------------Interfaces-------------------------

/*
Syntax:
    interface interface_name { }



*/
var person = {
     FirstName:"Tom", 
     LastName:"Hanks", 
     sayHi: ()=>{ 
         return "Hi"
        } 
    };

person.sayHi();
//------------------------------------------------------------
var customer = { 
    firstName: "Tom", 
    lastName: "Hanks", 
    sayHi: function () 
     { 
         return "Hi there";
       } 
}; 
console.log("Customer Object "); 
console.log(customer.firstName); 
console.log(customer.lastName); 
console.log(customer.sayHi()); 
var employee = { 
    firstName: "Jim", 
    lastName: "Blakes", 
    sayHi: function () 
        {     
           return "Hello!!!";
     } 
}; 
console.log("Employee Object "); 
console.log(employee.firstName); 
console.log(employee.lastName);


